const ParkMiller = require('./ParkMiller').ParkMiller;
const BlumBlumShub = require('./BlumBlumShub').BlumBlumShub;
const MersenneTwister = require('mersennetwister');
const { LaggedFibonacci } = require('./LaggedFibonacci');
const kstest = require( '@stdlib/stats-kstest' );
//var Plotly = require('plotly.js/lib/core');
const summary = require('summary');
const express = require('express')
const app = express()
const port = 3000
const path = require('path');

 app.listen(port);
 app.get('/', (req, res) => {
     res.sendFile(path.join(__dirname,'/index.html'))

 })
 app.post('/', (req,res) => {
    res.send(tab.parkMillerTab)
 })

let rnd = parseInt(Math.floor(Math.random() * 1000));
const parkMiller = new ParkMiller(rnd);
const laggedFib = new LaggedFibonacci(rnd, {})
const blumBlumShub = new BlumBlumShub();
const mersenneTwister = new MersenneTwister(rnd);
blumBlumShub.seed(rnd);
const nextBoundedInt= function(max) {
    return (max*rnd)/(this.initialize_var()+1)
}

const tab = {
    mersenneTwisterTab: [],
    laggedFibTab: [],
    blumBlumShubTab: [],
    parkMillerTab: []
};

for (let index = 0; index < 10000; index++) {
    let mersenneTwisterRnd = mersenneTwister.rnd();
    let blumBlumShubRnd = blumBlumShub.next();
     let parkMillerRnd = parkMiller.integer();
    let laggedFibRnd = laggedFib.next();
    tab.mersenneTwisterTab.push(mersenneTwisterRnd);
    tab.blumBlumShubTab.push(blumBlumShubRnd);
    tab.parkMillerTab.push(parkMillerRnd);
    tab.laggedFibTab.push(laggedFibRnd);
}

// console.log(tab.blumBlumShubTab);

const outMersenneTwister = kstest(tab.mersenneTwisterTab,'uniform',0.0,1.0);
const outBlumBlumShub = kstest(tab.blumBlumShubTab,'uniform',0.0,1.0);
const outParkMiller = kstest(tab.parkMillerTab,'uniform',0.0,1.0);
const outLaggedFib = kstest(tab.laggedFibTab,'uniform',0.0,1.0);

console.log('Mersenne Twister : '+outMersenneTwister.print()+'\n **************************** \n \n \n');
console.log('Blum Blum Shub '+outBlumBlumShub.print()+'\n **************************** \n \n \n');
console.log('Park Miller '+outParkMiller.print()+'\n **************************** \n \n \n');
console.log('Lagged Fibonacci '+outLaggedFib.print()+'\n ****************************');

const dataMersenneTwister = summary(tab.mersenneTwisterTab);
const dataBlumBlumShub = summary(tab.blumBlumShubTab);
const dataParkMiller = summary(tab.parkMillerTab);
const dataLaggedFib = summary(tab.laggedFibTab);

console.log('Mersenne Twister quartile 25% : '+dataMersenneTwister.quartile(0.25))
console.log('Blum Blum Shub quartile 25% : '+dataBlumBlumShub.quartile(0.25))
console.log('Park Miller quartile 25% : '+dataParkMiller.quartile(0.25))
console.log('Lagged Fib quartile 25% : '+dataLaggedFib.quartile(0.25))



  