class BlumBlumShub {
    constructor(rnd){
        this.x = undefined;
        this.rnd = rnd;
        var p = 5651;
        var q = 5623;
        this.M = p * q;
    }

    /** Get the gcd of two numbers, A and B. */
    gcd(a, b) {
        while(a != b) {
            if(a > b) {
                a = a - b;
            } else {
                b = b - a;
            }
        }
        return a;
    }
    
    /** Seed the random number generator. */
    seed(s) {
        if(s == 0) {
            throw new Error("The seed x[0] cannot be 0");
        } else if(s == 1) {
            throw new Error("The seed x[0] cannot be 1");
        } else if(this.gcd(s, this.M) != 1) {
            throw new Error("The seed x[0] must be co-prime to " + M.toString());
        } else {
            this.x = s;
            return s;
        }
    }
    
    /** Get next item from the random number generator. */
    next() {
        var cachedx = this.x;
        cachedx = cachedx * this.x;
        cachedx = cachedx % this.M;
        this.x = cachedx;
        
        return (1*this.x)/(this.M+1);
    }
}

module.exports = {
    BlumBlumShub
}
