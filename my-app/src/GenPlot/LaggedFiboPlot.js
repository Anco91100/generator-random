import React from "react";
import LaggedFibonacci from '../Gen/LaggedFibonacci';
import Histogram from "../Histogram";

class LaggedFibonnaciPlot extends React.Component {
    constructor(props){
        super(props);
        this.trace = this.trace.bind(this);
        this.layout = this.layout.bind(this);
        this.state = {
            laggedFibTab: []
        }
    }
    trace(){
        let rnd = parseInt(Math.floor(Math.random() * 1000));
        let laggedFib = new LaggedFibonacci(rnd, {});
        for (let i = 0; i < 10000; i++) {
            let laggedFibRnd = laggedFib.next();
            this.setState(prevState => ({
                laggedFibTab: [...prevState.laggedFibTab, laggedFibRnd]
              }))
        }
        return {
            x: this.state.laggedFibTab,
            type: 'histogram',
            marker: {
                color: 'orange',
             }
        }
    }

    layout(){
        return {

            bargap: 0.05, 
          
            bargroupgap: 0.2, 
          
            barmode: "overlay", 
            title: "Lagged Fibonnaci", 
          }
    }
    render(){
        return(
            <Histogram
            trace={this.trace()}
            layout={this.layout()}
            />
        )
    }
}

export default LaggedFibonnaciPlot;