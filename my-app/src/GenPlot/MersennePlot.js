import React from "react";
import MersenneTwister from "mersennetwister";
import Histogram from "../Histogram";

class MersennePlot extends React.Component {
    constructor(props){
        super(props);
        this.trace = this.trace.bind(this);
        this.layout = this.layout.bind(this);
        this.genTab = this.genTab.bind(this);
        this.state = {
            mersenneTab: []
        }
    }


    genTab(){
        let rnd = parseInt(Math.floor(Math.random() * 1000));
        let mersenneTwister = new MersenneTwister(rnd);
        let tab = []
        for (let i = 0; i < 10000; i++) {
            let mersenneTwisterRnd = mersenneTwister.rnd();
            tab.push(mersenneTwisterRnd);
        }
        return tab;
    }

    trace(){
        let tab = this.genTab();
        this.setState(prevState => ({
            mersenneTab: [...prevState.mersenneTab, ...tab]
          }))
        return {
            x: this.state.mersenneTab,
            type: 'histogram',
            marker: {
                color: 'green',
             }
        }
    }

    layout(){
        return {

            bargap: 0.05, 
          
            bargroupgap: 0.2, 
          
            barmode: "overlay", 
          
            title: "Mersenne Twisteter", 
          }
    }
    render(){
        return(
            <Histogram
            trace={this.trace()}
            layout={this.layout()}
            />
        )
    }
}

export default MersennePlot;