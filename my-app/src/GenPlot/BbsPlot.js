import React from "react";
import BlumBlumShub from '../Gen/BlumBlumShub';
import Histogram from "../Histogram";

class BlumBlumShubPlot extends React.Component {
    constructor(props){
        super(props);
        this.trace = this.trace.bind(this);
        this.layout = this.layout.bind(this);
        this.state = {
            bbsTab: []
        }
    }
    trace(){
        let rnd = parseInt(Math.floor(Math.random() * 1000));
        let blumBlumShub = new BlumBlumShub();
        blumBlumShub.seed(rnd);
        for (let i = 0; i < 10000; i++) {
            let blumBlumShubRnd = blumBlumShub.next();
            this.setState(prevState => ({
                bbsTab: [...prevState.bbsTab, blumBlumShubRnd]
              }))
        }
        return {
            x: this.state.bbsTab,
            type: 'histogram',
            marker: {
                color: 'purple',
             }
        }
    }

    layout(){
        return {

            bargap: 0.05, 
          
            bargroupgap: 0.2, 
          
            barmode: "overlay", 
          
            title: "BlumBlumShub", 
          }
    }
    render(){
        return(
            <Histogram
            trace={this.trace()}
            layout={this.layout()}
            />
        )
    }
}

export default BlumBlumShubPlot;