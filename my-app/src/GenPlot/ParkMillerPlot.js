import React from "react";
import ParkMiller from '../Gen/ParkMiller';
import Histogram from "../Histogram";

class ParkMillerPlot extends React.Component {
    constructor(props){
        super(props);
        this.trace = this.trace.bind(this);
        this.layout = this.layout.bind(this);
        this.state = {
            parkMillerTab: []
        }
    }
    trace(){
        let rnd = parseInt(Math.floor(Math.random() * 1000));
        let parkMiller = new ParkMiller(rnd);
        for (let i = 0; i < 100; i++) {
            let parkMillerRnd = parkMiller.integer();
            this.setState(prevState => ({
                parkMillerTab: [...prevState.parkMillerTab, parkMillerRnd]
              }))
        }
        return {
            x: this.state.parkMillerTab,
            type: 'histogram',
            marker: {
                color: 'red',
             }
        }
    }

    layout(){
        return {

            bargap: 0.05, 
          
            bargroupgap: 0.2, 
          
            barmode: "overlay", 
          
            title: "Park Miller", 
          }
    }
    render(){
        return(
            <Histogram
            trace={this.trace()}
            layout={this.layout()}
            />
        )
    }
}

export default ParkMillerPlot;