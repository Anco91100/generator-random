import React from "react";
import Plot from "react-plotly.js";

class Histogram extends React.Component {
    render(){
        return (
            <Plot 
            data={
                [this.props.trace]
            }
            layout={
                this.props.layout
            }
            />
        )
    }
}

export default Histogram;