import './App.css';
import MersennePlot from './GenPlot/MersennePlot';
import ParkMillerPlot from './GenPlot/ParkMillerPlot';
import BlumBlumShubPlot from './GenPlot/BbsPlot';
import LaggedFibonacci from './GenPlot/LaggedFiboPlot';

function App() {
  return (
    <div className="App">
      <MersennePlot />
      {/* <ParkMillerPlot />
      <BlumBlumShubPlot />
      <LaggedFibonacci /> */}
    </div>
  );
}

export default App;
